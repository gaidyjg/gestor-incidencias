<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        User::create([
            'name' => 'Administrador',
            'email' => 'admin@tovarsat.com.ve',
            'password' => bcrypt('adminadmin'),
            'role' => 0
        ]);

        // User
        User::create([
            'name' => 'User',
            'email' => 'user@tovarsat.com.ve',
            'password' => bcrypt('useruser'),
            'role' => 1
        ]);
    }
}
