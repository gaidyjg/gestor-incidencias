<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="{{ asset('css/dup/img/fav.png') }}">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Gestion Corporativos</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,300,500" rel="stylesheet">
<!-- CSS ============================================= -->
	<link rel="stylesheet" href="{{ asset('css/dup/css/linearicons.css') }}"> 
	<link rel="stylesheet" href="{{ asset('css/dup/css/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('css/dup/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/dup/css/nice-select.css') }}">
	<link rel="stylesheet" href="{{ asset('css/dup/css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('css/dup/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('css/dup/css/main.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/vendor/animate/animate.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/vendor/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/vendor/animsition/css/animsition.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/vendor/select2/select2.min.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/vendor/daterangepicker/daterangepicker.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/login/css/main.css') }}">
<!--===============================================================================================-->
	</head>
	<body class="dup-body">
		<div class="dup-body-wrap">
			<!-- Start Header Area -->
			<header class="default-header">
				<div class="header-wrap">
					<div class="header-top d-flex justify-content-between align-items-center">
						<div class="logo">
							<a href="#"><img src="{{ asset('img/logo.png') }}" width="145" height="45" alt=""></a>
						</div>
						<!--<div class="main-menubar d-flex align-items-center">
							<nav class="hide">
								<a href="index.html">Home</a>
								<a href="generic.html">Generic</a>
								<a href="elements.html">Elements</a>
							</nav>
							<div class="menu-bar"><span class="lnr lnr-menu"></span></div>
						</div>-->
					</div>
				</div>
			</header>
		<!-- End Header Area -->
		<!-- Start Banner Area -->
		<section class="banner-area relative">
			<div class="overlay overlay-bg"></div>
			<div class="container">
				<div class="row fullscreen align-items-center justify-content-between">
					<div class="col-lg-6 col-md-7 col-sm-8">
						<div class="banner-content">
							<h1>Sistema de Gestión <br> a Corporativos.</h1>

							<form class="login100-form validate-form p-b-0 p-t-0 method=" method="POST" action="{{ route('login') }} ">
								@csrf
								<div class="wrap-input100 validate-input" data-validate = "Enter username">
									<input class="input100" type="text" name="email" placeholder="Correo Corporativo" value="{{ old('email') }}" autocomplete="email" autofocus>
									<span class="focus-input100" data-placeholder="&#xe82a;"></span>
								</div>
								@error('email')
                                    <span class="validate-input" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

								<div class="wrap-input100 validate-input" data-validate="Enter password">
									<input class="input100" type="password" name="password" placeholder="Contraseña" autocomplete="current-password">
									<span class="focus-input100" data-placeholder="&#xe80f;"></span>
								</div>

								@error('password')
                                    <span class="validate-input" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
								@enderror
								
								<button type="submit" class="primary-btn">
									Acceder<span class="lnr lnr-arrow-right"></span>
								</button>
							</form>
							<!--<a href="#" class="primary-btn">Get Started<span class="lnr lnr-arrow-right"></span></a>-->
						</div>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-4">
						<img src="{{ asset('img/analytics.svg') }}" alt="" class="img-fluid">
					</div>
				</div>
			</div>
		</section>
        
	<script src="{{ asset('css/dup/js/vendor/jquery-2.2.4.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="{{ asset('css/dup/js/vendor/bootstrap.min.js') }}"></script>
	<script src="{{ asset('css/dup/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('css/dup/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('css/dup/js/jquery.nice-select.min.js') }}"></script>
	<script src="{{ asset('css/dup/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('css/dup/js/main.js') }}"></script>

</body>
</html>